$.write("start\n")

// Define function to get only the filename
var getFilename = function(str) {
    return str.split('\\').pop().split('/').pop();
}

// Define input/output folder
// Folder containing all filelists
var filelistFolder = new Folder("/Users/ba/repos/dv_video_concat/filelists");
var allFilelists = filelistFolder.getFiles()
// Folder where all exported video should go
var exportFolder = "/Volumes/Archive 1/Video/Projects/JVC/"
// Path to the Adobe Media Encoder preset file (included in repository)
var eprPath = "/Users/ba/repos/dv_video_concat/extendscript/jvc.epr"
// Path where all the Premiere Pro project files should be stored
var projectPath = "/Users/ba/Movies/Projects/JVC/"

$.write("Found " + allFilelists.length + " files in folder " + filelistFolder + "\n")

// Read list of files
for (var f = 0; f < allFilelists.length; f++) {
    // Get the path and name
    filelistPath = allFilelists[f].toString();
    filelistName = getFilename(allFilelists[f].toString());
    $.write(filelistName + "\n")
    // Skip the .DS_Store file
    if (filelistName == ".DS_Store") {
        continue;
    } else {
        // Open the file
        var dataFile = new File(filelistPath);
        dataFile.open("r");

        // Put every line into an array item
        videos = [];
        while (dataFile.eof == false) {
            videos.push(dataFile.readln());
        }
        // Close filelist
        dataFile.close();
    }

    // INTERFACING WITH PREMIERE
    // Create the project
    app.newProject(projectPath + filelistName + ".prproj")

    // Loop over all files found in filelist and import every videos into project
    for (var i = 0; i < videos.length; i++) {
        video = videos[i].toString()
        $.write("\nImporting " + i + " " + video)
        app.project.importFiles([video], true, app.project.rootItem, false)
    }

    // create array of clips that should be in the sequence
    clips = []
    for (var c = 0; c < app.project.rootItem.children.length; c++) {
        clips.push(app.project.rootItem.children[c])
    }

    // Create sequence from clips
    app.project.createNewSequenceFromClips("newSequence", clips);

    // Export (queue in Media Encoder)
    app.encoder.encodeSequence(app.project.sequences[0], exportFolder + filelistName + ".mp4", eprPath, 1, 0)

    // Close project and save it
    app.project.closeDocument(1, 0)

    $.write("\n\n ---- Done processing file list " + filelistName + " ---- \n")
}