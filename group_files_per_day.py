import datetime as dt
import os
import glob

# The parent path of all videos
path = "/Volumes/Archive 1/Video/Footage/JVC [REC]/2005/Scenes/"
# Glob pattern
pattern = f"{glob.escape(path)}*.mov"
# Output path of the filelist text file
outpath = "/Users/ba/repos/dv_video_concat/filelists/"

# Get all files matching the patter
files = glob.glob(pattern)

# For all files get the modification time and sort by it
finfo = [[file, os.path.getmtime(file)] for file in files]
finfo.sort(key=lambda x: x[1])

# Get unique days in YYYY-mm-dd format
cdates = list(set([f"{dt.datetime.fromtimestamp(file[1]):%Y-%m-%d}" for file in finfo]))

# For every unique date go through all files
for cdate in cdates:
    print(cdate)
    f = open(f"{outpath+cdate}", "a")
    for file in finfo:
        # If the date of this file matches the current unique date, append filename to the txt file
        if f"{dt.datetime.fromtimestamp(file[1]):%Y-%m-%d}" == cdate:
            f.write(f"{file[0]}\n")
        else:
            continue
    f.close()

print("done")

