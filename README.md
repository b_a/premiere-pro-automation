# Introduction

The purpose of the scripts in this repository is to compile individual clips from a DV camera into a single video per day.  
E.g. if there are 5 clips: 

* `DV_001.mov` recorded on 2007-08-09
* `DV_002.mov` recorded on 2007-08-09
* `DV_003.mov` recorded on 2007-08-09
* `DV_004.mov` recorded on 2009-01-10
* `DV_005.mov` recorded on 2009-01-10

The output will consist of two videos:

* `2007-08-09.mp4`, a concatenation of videos `DV_001` to `DV_003`
* `2009-01-10.mp4`, a concatenation of videos `DV_004` and `DV_005`


# Requirements

* [Adobe Premiere Pro >= 14.4][2]
* [Python 3][3]
* [Visual Studio Code][4]
* [ExtendScript Debugger][5]

# Steps to run the scripts

1. Open Python script [`group_files_per_day.py`][8] and set appropriate values for the `path` and `outpath` variable.
2. Run the Python script [`group_files_per_day.py`][8]. The folder defined in `outpath` should now have one or more filelists.
3. Open the ExtendScript file [`importer.jsx`][7] and set the appropriate values for the `filelistFolder`, `exportFolder`, `eprPath` and `projectPath` variables.
4. Open Premiere Pro
5. Open [`importer.jsx`][7] in Visual Studio Code
6. Select Premiere Pro as target application (see [ExtendScript Debugger documentation][5] for more information)
7. Run the [`importer.jsx`][7] script, this will:
   * read every filelist in the folder `filelistFolder`
   * create a Premiere Pro project per filelist
   * import all the videos in a filelist into the project
   * queue the export in Adobe Media Encoder
   * close the Premiere Pro project
8. In the end Adobe Media Encoder will have a list of encoding jobs, which can be completed to get the output.

# Other

* [`concat_videos.py`][9] is an attempt to replace Premiere Pro and use [`ffmpeg`][6] instead to concatenate the videos. Unfortunately `ffmpeg` requires far more configuration to reliably read DV camcorder files compared to Premiere Pro, therefore this script is abandoned.

[1]: https://premiere-scripting-guide.readthedocs.io/
[2]: https://www.adobe.com/products/premiere.html
[3]: https://www.python.org/download/releases/3.0/
[4]: https://code.visualstudio.com/
[5]: https://marketplace.visualstudio.com/items?itemName=Adobe.extendscript-debug
[6]: https://ffmpeg.org/
[7]: https://gitlab.com/b_a/premiere-pro-automation/-/blob/master/extendscript/importer.jsx
[8]: https://gitlab.com/b_a/premiere-pro-automation/-/blob/master/group_files_per_day.py
[9]: https://gitlab.com/b_a/premiere-pro-automation/-/blob/master/concat_videos.py