import glob
import os
import subprocess

# The parent path of all videos
path = "/Volumes/Archive 1/Video/Footage/JVC [REC]/2005/Scenes/"
# Glob pattern
pattern = f"{glob.escape(path)}*.txt"

# Get a list of .txt files containing video files that are recorded on the same day
video_lists = glob.glob(pattern)

# Change directory
os.chdir(path)

# For every .txt file
for video_list in video_lists:
    # Define an output filename
    outname = video_list.replace(".txt", ".mp4")
    # Put together ffmpeg command
    command = ["ffmpeg", "-safe", "0", "-f", "concat", "-i", f"{video_list}",
               "-vf", "scale=768x576,setsar=1:1,yadif", f"{outname}"]
    print(command)
    # Only run command if file doesn't exist already
    if not os.path.exists(outname):
        subprocess.call(command)
    else:
        print(f"{outname} exists already.")
